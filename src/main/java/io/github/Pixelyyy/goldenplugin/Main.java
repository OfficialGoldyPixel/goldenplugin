package io.github.Pixelyyy.goldenplugin;

import java.util.logging.Logger;

import io.github.Pixelyyy.goldenplugin.commands.*;
import org.bukkit.Server;
import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.java.JavaPlugin;

import io.github.Pixelyyy.goldenplugin.commands.*;
import io.github.Pixelyyy.goldenplugin.events.EventListener;

/*
 * GoldenPlugin
 * Copyright (c) GoldenPixel 2020
 */
public class Main extends JavaPlugin {
	Logger logger = Logger.getLogger("GOLDENPLUGIN");
	private static boolean isServerOnMaintenance = false;

	public static Server server;

	public static String[] PUBLICPERMISSIONS = {
			"goldenplugin.public.helloworld.use",
			"goldenplugin.public.spawn.use",
			"goldenplugin.public.cs.use",
			"goldenplugin.public.stonekit.use",
			"goldenplugin.public.tr.use",
			"goldenplugin.public.onlinepl.use",
			"goldenplugin.public.ironkit.use",
			"goldenplugin.public.playermap.use",
			"goldenplugin.public.enderchest.use"
	};

	public static String[] PRIVATEPERMISSIONS = {
			"goldenplugin.private.alert.use",
			"goldenplugin.private.cc.use",
			"goldenplugin.private.ss.use",
			"goldenplugin.private.up.use",
			"goldenplugin.private.rtp.use",
			"goldenplugin.private.admin.use",
			"goldenplugin.private.rsc.use",
			"goldenplugin.private.note.use",
			"goldenplugin.private.torch.use",
			"goldenplugin.private.unbreaker.use",
			"goldenplugin.private.kill.use",
			"goldenplugin.private.seth.use",
			"goldenplugin.private.geth.use",
			"goldenplugin.private.lock.use",
			"goldenplugin.private.heal.use",
			"goldenplugin.private.feed.use",
			"goldenplugin.private.dispatch.use",
			"goldenplugin.private.ublock.use",
			"goldenplugin.private.megabow.use",
			"goldenplugin.private.pvp.use",
			"goldenplugin.private.disappear.use",
			"goldenplugin.private.dirt.use",
			"goldenplugin.private.group.use",
			"goldenplugin.private.unsaturate.use",
			"goldenplugin.private.prefix.use"
	};
	
	@Override
	public void onDisable() {
		System.out.println("Stopping GoldenPlugin");
		EventListener.plMap.clear();
		System.out.println("Cleared Player Hashmap");
	}

	@SuppressWarnings("all")
	@Override
	public void onEnable() {
		// Load config
		saveDefaultConfig();

		// Load commands
		getCommand("helloworld").setExecutor(new HelloWorld());
		getCommand("alert").setExecutor(new Alert());
		getCommand("cc").setExecutor(new Clear());
		getCommand("supersword").setExecutor(new SuperSword());
		getCommand("spawn").setExecutor(new Spawn());
		getCommand("up").setExecutor(new Up());
		getCommand("randomtp").setExecutor(new RandomTP());
		getCommand("godmode").setExecutor(new invincible());
		getCommand("ungodmode").setExecutor(new DisInvincible());
		getCommand("admin").setExecutor(new AdminMode());
		getCommand("cstorage").setExecutor(new CommunityStorage());
		getCommand("inventorysee").setExecutor(new InventorySee());
		getCommand("enderchest").setExecutor(new EnderChestSee());
		getCommand("stonekit").setExecutor(new StoneKit());
		getCommand("tr").setExecutor(new TrashInventory());
		getCommand("rservercheck").setExecutor(new ResponseCheck());
		getCommand("note").setExecutor(new Note());
		getCommand("notes").setExecutor(new Notes());
		getCommand("rnote").setExecutor(new NoteRemove());
		getCommand("ironkit").setExecutor(new IronKit());
		getCommand("torch").setExecutor(new Torch());
		getCommand("unbreakable").setExecutor(new Unbreakable());
		getCommand("onlinepl").setExecutor(new OnlinePlayers());
		getCommand("silentkill").setExecutor(new Kill());
		getCommand("seth").setExecutor(new SetHealth());
		getCommand("geth").setExecutor(new GetHealth());
		getCommand("lock").setExecutor(new Maintenance());
		getCommand("heal").setExecutor(new Heal());
		getCommand("feed").setExecutor(new Feed());
		getCommand("sudo").setExecutor(new Sudo());
		getCommand("ublock").setExecutor(new BlockUnderMe());
		getCommand("playermap").setExecutor(new PlayerMap());
		getCommand("megabow").setExecutor(new MegaBow());
		getCommand("pvp").setExecutor(new PVP());
		getCommand("disappear").setExecutor(new Disappear());
		getCommand("group").setExecutor(new GroupManager());
		getCommand("unsaturate").setExecutor(new Unsaturate());
		getCommand("prefix").setExecutor(new Prefix(this));

		// Load events
		getServer().getPluginManager().registerEvents(new EventListener(this), this);

		// Assign variables
		server = getServer();
	}
	
	@Override
	public void onLoad() {

	}

	public static void maintenance(boolean b) {
		isServerOnMaintenance = b;
	}

	public static boolean isOnMaintenance() {
		return isServerOnMaintenance;
	}

	public void setCommand(String name, CommandExecutor exec) {
		this.getCommand(name).setExecutor(exec);
	}
}