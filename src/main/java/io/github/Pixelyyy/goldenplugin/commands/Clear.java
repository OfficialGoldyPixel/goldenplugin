package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Clear implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		arg0.sendMessage("Clearing chat...");
		
		if(arg0 instanceof Player) {
			Player sender = (Player)arg0;
			if (GroupManager.isInGroup(sender, "admin")) {
				Bukkit.broadcastMessage(sender.getName() + " à demandé de vider le chat.");

				for(int i = 0; i < 100; i++) {
					Bukkit.broadcastMessage(" ");
				}
			} else {
				sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			Bukkit.broadcastMessage("La console à demandé de vider le chat.");

			for(int i = 0; i < 100; i++) {
				Bukkit.broadcastMessage(" ");
			}
		}
		

		
		return true;
	}

}
