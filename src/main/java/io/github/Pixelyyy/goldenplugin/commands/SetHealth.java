package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetHealth implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        try {
            if (commandSender instanceof Player) {
                Player pl = (Player)commandSender;
                if (GroupManager.isInGroup(pl, "superadmin")) {
                    String askedPlName = strings[0];
                    double askedH = Double.parseDouble(strings[1]);

                    Bukkit.getPlayer(askedPlName).setHealth(askedH);
                } else {
                    pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
                }
            } else {
                String askedPlName = strings[0];
                double askedH = Double.parseDouble(strings[1]);

                Bukkit.getPlayer(askedPlName).setHealth(askedH);
            }
        } catch(ArrayIndexOutOfBoundsException e) {
            commandSender.sendMessage("Pas assez ou trop d'arguments! Utilisation : /seth <joueur> <nombre>");
        } catch(NullPointerException e) {
            commandSender.sendMessage("Ce joueur n'existe pas ou n'est pas en ligne!");
        } catch(IllegalArgumentException e) {
            commandSender.sendMessage("La vie donnée doit être entre 0.0 et 20.0! (sauf exception)");
        }
        return false;
    }
}
