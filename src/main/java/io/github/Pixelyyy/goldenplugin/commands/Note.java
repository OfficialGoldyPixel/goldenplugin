package io.github.Pixelyyy.goldenplugin.commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Note implements CommandExecutor {

	public static ArrayList<String> noteTable = new ArrayList<String>();
	
	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if (arg0 instanceof Player) {
			Player pl = (Player)arg0;
			if (GroupManager.isInGroup(pl, "vip")) {
				StringBuilder bc = new StringBuilder();

				for(String str : arg3) {
					bc.append(str + " ");
				}

				noteTable.add(bc.toString());
			} else {
				pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			StringBuilder bc = new StringBuilder();

			for(String str : arg3) {
				bc.append(str + " ");
			}

			noteTable.add(bc.toString());
		}
		return false;
	}

}
