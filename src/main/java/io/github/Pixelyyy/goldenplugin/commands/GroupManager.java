package io.github.Pixelyyy.goldenplugin.commands;

import io.github.Pixelyyy.goldenplugin.events.EventListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;

/**
 * This class is very long.
 */
public class GroupManager implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        try {
            if (commandSender instanceof Player) {
                Player pl = (Player)commandSender;

                if (isInGroup(pl, "superadmin")) {
                    if(strings[0].equals("add")) {
                        if(strings[1].equals("vip")) {
                            addToGroup(Bukkit.getPlayer(strings[2]), "vip");
                        }

                        if(strings[1].equals("admin")) {
                            addToGroup(Bukkit.getPlayer(strings[2]), "admin");
                        }

                        if(strings[1].equals("superadmin")) {
                            addToGroup(Bukkit.getPlayer(strings[2]), "superadmin");
                        }

                        if(strings[1].equals("dev")) {
                            addToGroup(Bukkit.getPlayer(strings[2]), "dev");
                        }
                    }

                    if(strings[0].equals("remove")) {
                        if(strings[1].equals("vip")) {
                            removeFromGroup(Bukkit.getPlayer(strings[2]), "vip");
                        }

                        if(strings[1].equals("admin")) {
                            removeFromGroup(Bukkit.getPlayer(strings[2]), "admin");
                        }

                        if(strings[1].equals("superadmin")) {
                            removeFromGroup(Bukkit.getPlayer(strings[2]), "superadmin");
                        }

                        if(strings[1].equals("dev")) {
                            removeFromGroup(Bukkit.getPlayer(strings[2]), "dev");
                        }
                    }

                    if(strings[0].equals("check")) {
                        if(isInGroup(Bukkit.getPlayer(strings[2]), strings[1])) {
                            commandSender.sendMessage(strings[2] + " est bien dans le groupe " + strings[1]);
                        } else {
                            commandSender.sendMessage(strings[2] + " n'est pas dans le groupe " + strings[1]);
                        }
                    }
                } else {
                    pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
                }
            } else {
                if(strings[0].equals("add")) {
                    if(strings[1].equals("vip")) {
                        addToGroup(Bukkit.getPlayer(strings[2]), "vip");
                    }

                    if(strings[1].equals("admin")) {
                        addToGroup(Bukkit.getPlayer(strings[2]), "admin");
                    }

                    if(strings[1].equals("superadmin")) {
                        addToGroup(Bukkit.getPlayer(strings[2]), "superadmin");
                    }

                    if(strings[1].equals("dev")) {
                        addToGroup(Bukkit.getPlayer(strings[2]), "dev");
                    }
                }

                if(strings[0].equals("remove")) {
                    if(strings[1].equals("vip")) {
                        removeFromGroup(Bukkit.getPlayer(strings[2]), "vip");
                    }

                    if(strings[1].equals("admin")) {
                        removeFromGroup(Bukkit.getPlayer(strings[2]), "admin");
                    }

                    if(strings[1].equals("superadmin")) {
                        removeFromGroup(Bukkit.getPlayer(strings[2]), "superadmin");
                    }

                    if(strings[1].equals("dev")) {
                        removeFromGroup(Bukkit.getPlayer(strings[2]), "dev");
                    }
                }

                if(strings[0].equals("check")) {
                    if(isInGroup(Bukkit.getPlayer(strings[2]), strings[1])) {
                        commandSender.sendMessage(strings[2] + " est bien dans le groupe " + strings[1]);
                    } else {
                        commandSender.sendMessage(strings[2] + " n'est pas dans le groupe " + strings[1]);
                    }
                }
            }
        } catch (NullPointerException e) {
            commandSender.sendMessage("Ce joueur n'existe pas ou n'est pas en ligne!");
        } catch(ArrayIndexOutOfBoundsException e) {
            commandSender.sendMessage("Pas assez d'arguments! Utilisation : /group <add|remove|check> <groupe> <joueur>");
        }

        return false;
    }

    public static void removeFromGroup(Player pl, String group) {
        switch(group) {
            default:
                System.err.println("The group " + group + " does'nt exist!");
                break;
            case "vip":
                EventListener.vipGroup.put(pl, false);
                break;
            case "admin":
                EventListener.adminGroup.put(pl, false);
                break;
            case "superadmin":
                EventListener.superAdminGroup.put(pl, false);
                break;
            case "dev":
                EventListener.devGroup.put(pl, false);
                break;
        }
    }

    public static void addToGroup(Player pl, String group) {
        switch(group) {
            default:
                System.err.println("The group " + group + " does'nt exist!");
                break;
            case "vip":
                EventListener.vipGroup.put(pl, true);
                break;
            case "admin":
                EventListener.adminGroup.put(pl, true);
                break;
            case "superadmin":
                EventListener.superAdminGroup.put(pl, true);
                break;
            case "dev":
                EventListener.devGroup.put(pl, true);
                break;
        }
    }

    public static void addToGroups(Player pl, String[] groups) {
        for(String str : groups) {
            addToGroup(pl, str);
        }
    }

    public static boolean isInGroup(Player pl, String group) {
        switch(group) {
            case "vip":
                for(Map.Entry<Player, Boolean> e : EventListener.vipGroup.entrySet()) {
                    if(e.getKey() == pl) {
                        if(e.getValue()) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
                break;
            case "admin":
                for(Map.Entry<Player, Boolean> e : EventListener.adminGroup.entrySet()) {
                    if(e.getKey() == pl) {
                        if(e.getValue()) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
                break;
            case "superadmin":
                for(Map.Entry<Player, Boolean> e : EventListener.superAdminGroup.entrySet()) {
                    if(e.getKey() == pl) {
                        if(e.getValue()) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
                break;
            case "dev":
                for(Map.Entry<Player, Boolean> e : EventListener.devGroup.entrySet()) {
                    if(e.getKey() == pl) {
                        if(e.getValue()) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
                break;
            default:
                System.err.println("The group " + group + " does'nt exist!");
                return false;
        }

        return false;
    }
}
