package io.github.Pixelyyy.goldenplugin.commands;

import io.github.Pixelyyy.goldenplugin.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import io.github.Pixelyyy.goldenplugin.events.EventListener;

public class OnlinePlayers implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		arg0.sendMessage("Il y a " + EventListener.onlinePl.size() + " sur " + Main.server.getMaxPlayers() + " joueurs : " + EventListener.onlinePl.toString());
		return false;
	}

}
