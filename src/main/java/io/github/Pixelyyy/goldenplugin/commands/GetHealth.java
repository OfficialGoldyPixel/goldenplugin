package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class GetHealth implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        try {
            if(commandSender instanceof Player) {
                Player pl = (Player)commandSender;

                if (GroupManager.isInGroup(pl, "vip")) {
                    DecimalFormat f = new DecimalFormat("##.#");

                    String askedPlName = strings[0];
                    double health = Bukkit.getPlayer(askedPlName).getHealth();
                    String healthS = f.format(health);
                    commandSender.sendMessage("La vie de " + askedPlName + " est ---> " + ChatColor.RED + healthS + ChatColor.RESET);
                } else {
                    pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
                }
            } else {
                DecimalFormat f = new DecimalFormat("##.#");

                String askedPlName = strings[0];
                double health = Bukkit.getPlayer(askedPlName).getHealth();
                String healthS = f.format(health);
                commandSender.sendMessage("La vie de " + askedPlName + " est ---> " + ChatColor.RED + healthS + ChatColor.RESET);
            }
        } catch(ArrayIndexOutOfBoundsException e) {
            commandSender.sendMessage("Pas assez ou trop d'arguments! Utilisation : /geth <joueur>");
        } catch(NullPointerException e) {
            commandSender.sendMessage("Ce joueur n'existe pas ou n'est pas en ligne!");
        }

        return false;
    }
}
