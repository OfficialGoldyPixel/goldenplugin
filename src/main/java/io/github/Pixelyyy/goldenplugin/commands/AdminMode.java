package io.github.Pixelyyy.goldenplugin.commands;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class AdminMode implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		
		if(arg0 instanceof Player) {
			Player pl = (Player)arg0;

			if (GroupManager.isInGroup(pl, "admin")) {
				ItemStack adminstick = new ItemStack(Material.DIAMOND_AXE, 1);
				ItemMeta adminstickmeta = adminstick.getItemMeta();

				adminstickmeta.setDisplayName("Mode Admin");
				adminstickmeta.setLore(Arrays.asList("Activer le mode admin"));
				adminstickmeta.addEnchant(Enchantment.DAMAGE_ALL, 1, true);

				adminstickmeta.setUnbreakable(true);

				adminstick.setItemMeta(adminstickmeta);

				pl.getInventory().addItem(adminstick);
			} else {
				pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			System.out.println("Cette commande n'est pas éxécutable dans la Live Console.");
		}
		
		return false;
	}

}
