package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Torch implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		
		if(arg0 instanceof Player) {
			Player pl = (Player)arg0;
			if (GroupManager.isInGroup(pl, "vip")) {
				ItemStack i = new ItemStack(Material.TORCH, 30);
				pl.getInventory().addItem(i);
				pl.updateInventory();
			} else {
				pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			System.out.println("Cette commande n'est pas éxécutable depuis la Live Console.");
		}
		
		return false;
	}

}
