package io.github.Pixelyyy.goldenplugin.commands;

import io.github.Pixelyyy.goldenplugin.events.EventListener;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TrashInventory implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if(arg0 instanceof Player) {
			Player pl = (Player)arg0;
			
			pl.openInventory(EventListener.trash);
			EventListener.trash.clear();
		} else {
			System.out.println("Cette commande n'est pas éxécutable dans la Live Console.");
		}
		return false;
	}

}
