package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NoteRemove implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if (arg0 instanceof Player) {
			Player pl = (Player)arg0;
			if (GroupManager.isInGroup(pl, "vip")) {
				if(arg3.length == 1) {
					Note.noteTable.remove(Integer.parseInt(arg3[0]));
				} else {
					arg0.sendMessage("Pas assez ou trop d'arguments! Utilisation : /rnote <numéro>");
				}
			} else {
				pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			if(arg3.length == 1) {
				Note.noteTable.remove(Integer.parseInt(arg3[0]));
			} else {
				arg0.sendMessage("Pas assez ou trop d'arguments! Utilisation : /rnote <numéro>");
			}
		}

		return false;
	}

}
