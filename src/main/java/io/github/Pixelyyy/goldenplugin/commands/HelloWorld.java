package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HelloWorld implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		arg0.sendMessage("Hello World!");
		
		if(!(arg0 instanceof Player)) {
			arg0.sendMessage("Va-t-en de la Live Console!");
		} else {
			Player player = (Player)arg0;
			player.sendMessage("Regarde ça, moi aussi je peut envoyer des messages!");
			try {
				Thread.sleep(3000L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			player.chat("J'ai effectué la commande Hello World qui était, à la base fait pour le développement.");
		}
		
		return true;
	}

}
