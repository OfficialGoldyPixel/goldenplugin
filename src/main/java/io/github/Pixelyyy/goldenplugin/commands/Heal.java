package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Heal implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player) {
            Player pl = (Player)commandSender;
            if (GroupManager.isInGroup(pl, "admin")) {
                try {
                    Bukkit.getPlayer(strings[0]).setHealth(20.0);
                    commandSender.sendMessage(ChatColor.GREEN + "Vous avez soigné " + strings[0] + ChatColor.RESET);
                } catch(NullPointerException e) {
                    commandSender.sendMessage("Ce joueur n'existe pas ou n'est pas en ligne!");
                } catch(ArrayIndexOutOfBoundsException e) {
                    pl.setHealth(20.0d);
                    pl.sendMessage(ChatColor.GREEN + "Que la vie soit de ton coté!" + ChatColor.RESET);
                }
            } else {
                pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
            }
        } else {
            System.out.println("Cette commande n'est pas éxécutable depuis la Live Console");
        }
        return false;
    }
}
