package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Alert implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if(arg3.length == 0) {
			arg0.sendMessage("Pas assez d'arguments! Utilisation: \"/alert <texte> \"");
			return false;
		} else {
			if (arg0 instanceof Player) {
				Player pl = (Player)arg0;

				if (GroupManager.isInGroup(pl, "admin")) {
					StringBuilder bc = new StringBuilder();

					for(String part : arg3) {
						bc.append(part + " ");
					}

					Bukkit.broadcastMessage("[" + pl.getName() + "] !>> " + ChatColor.RED + bc.toString() + ChatColor.RESET);
				} else {
					pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
				}
			} else {
				StringBuilder bc = new StringBuilder();

				for(String part : arg3) {
					bc.append(part + " ");
				}

				Bukkit.broadcastMessage("[SERVEUR] !>> " + ChatColor.RED + bc.toString() + ChatColor.RESET);
			}
		}

		return false;
	}

}
