package io.github.Pixelyyy.goldenplugin.commands;

import io.github.Pixelyyy.goldenplugin.events.EventListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;

public class Sudo implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        try {
            if(commandSender instanceof Player) {
                Player pl = (Player)commandSender;
                    if(GroupManager.isInGroup(pl, "superadmin")) {
                        String requestedPlName = strings[0];

                        StringBuilder bc = new StringBuilder();
                        for(int i = 1; i < strings.length; i++) {
                            bc.append(strings[i] + " ");
                        }

                        if(!requestedPlName.equals("*")) {
                                Bukkit.getPlayer(requestedPlName).chat(bc.toString());

                                commandSender.sendMessage("Vous avez fait dire à " + requestedPlName + " : " + bc.toString());
                        } else {
                            for(Map.Entry<String, Player> element : EventListener.plMap.entrySet()) {
                                    element.getValue().chat(bc.toString());
                            }

                            commandSender.sendMessage("Vous avez fait dire à tout le monde : " + bc.toString());
                        }
                    } else {
                        pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
                    }
            } else {
                String requestedPlName = strings[0];

                StringBuilder bc = new StringBuilder();
                for(int i = 1; i < strings.length; i++) {
                    bc.append(strings[i] + " ");
                }

                if(!requestedPlName.equals("*")) {
                    Bukkit.getPlayer(requestedPlName).chat(bc.toString());

                    commandSender.sendMessage("Vous avez fait dire à " + requestedPlName + " : " + bc.toString());
                } else {
                    for(Map.Entry<String, Player> element : EventListener.plMap.entrySet()) {
                        element.getValue().chat(bc.toString());
                    }

                    commandSender.sendMessage("Vous avez fait dire à tout le monde : " + bc.toString());
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            commandSender.sendMessage("Pas assez d'arguments! Utilisation : /sudo <joueur> <message> (voir plus dans les logs)");
            e.printStackTrace();
        } catch(NullPointerException e) {
            commandSender.sendMessage("Ce joueur n'existe pas ou n'est pas en ligne! (voir plus dans les logs)");
            e.printStackTrace();
        }

        return false;
    }
}
