package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class StoneKit implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if(arg0 instanceof Player) {
			Player pl = (Player)arg0;
			
			ItemStack sword = new ItemStack(Material.STONE_SWORD);
			ItemMeta swordM = sword.getItemMeta();
			
			swordM.setDisplayName("L'épée en pierre");
			swordM.setUnbreakable(true);
			sword.setItemMeta(swordM);
			
			ItemStack pickaxe = new ItemStack(Material.STONE_PICKAXE);
			ItemMeta pickaxeM = sword.getItemMeta();
			
			pickaxeM.setDisplayName("La pioche de pierre");
			pickaxeM.setUnbreakable(true);
			pickaxe.setItemMeta(pickaxeM);
			
			ItemStack axe = new ItemStack(Material.STONE_AXE);
			ItemMeta axeM = sword.getItemMeta();
			
			axeM.setDisplayName("La hache de pierre");
			axeM.setUnbreakable(true);
			axe.setItemMeta(axeM);
			
			ItemStack shovel = new ItemStack(Material.STONE_SHOVEL);
			ItemMeta shovelM = sword.getItemMeta();
			
			shovelM.setDisplayName("La pelle de pierre");
			shovelM.setUnbreakable(true);
			shovel.setItemMeta(shovelM);
			
			ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
			ItemMeta helmetM = helmet.getItemMeta();
			
			helmetM.setDisplayName("Casque en cuir");
			helmetM.setUnbreakable(true);
			helmet.setItemMeta(helmetM);
			
			ItemStack tunic = new ItemStack(Material.LEATHER_CHESTPLATE);
			ItemMeta tunicM = tunic.getItemMeta();
			
			tunicM.setDisplayName("Tunique en cuir");
			tunicM.setUnbreakable(true);
			tunic.setItemMeta(tunicM);
			
			ItemStack pants = new ItemStack(Material.LEATHER_LEGGINGS);
			ItemMeta pantsM = pants.getItemMeta();
			
			pantsM.setDisplayName("Pantalons en cuir");
			pantsM.setUnbreakable(true);
			pants.setItemMeta(pantsM);
			
			ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
			ItemMeta bootsM = pants.getItemMeta();
			
			bootsM.setDisplayName("Bottes en cuir");
			bootsM.setUnbreakable(true);
			boots.setItemMeta(bootsM);
			
			ItemStack shield = new ItemStack(Material.SHIELD);
			ItemMeta shieldM = shield.getItemMeta();
			
			shieldM.setDisplayName("Le bouclier de la mort");
			shieldM.setUnbreakable(true);
			shield.setItemMeta(shieldM);
			
			ItemStack potato = new ItemStack(Material.BAKED_POTATO, 5);
			
			pl.getInventory().addItem(sword);
			pl.getInventory().addItem(pickaxe);
			pl.getInventory().addItem(axe);
			pl.getInventory().addItem(shovel);
			pl.getInventory().addItem(potato);
			
			pl.getInventory().addItem(helmet);
			pl.getInventory().addItem(tunic);
			pl.getInventory().addItem(pants);
			pl.getInventory().addItem(boots);
			
			pl.getInventory().addItem(shield);
			
			pl.sendMessage("§eWhoosh!");
		} else {
			System.out.println("Cette commande n'est pas éxécutable depuis la Live Console");
		}
		
		return false;
	}

}
