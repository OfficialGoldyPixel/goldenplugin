package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class invincible implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		
		if(arg0 instanceof Player) {
			Player pl = (Player)arg0;
			if (GroupManager.isInGroup(pl, "admin")) {
				pl.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 100));
				pl.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 100));
				pl.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, Integer.MAX_VALUE, 100));
				pl.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 100));
				pl.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 100));
				pl.addPotionEffect(new PotionEffect(PotionEffectType.DOLPHINS_GRACE, Integer.MAX_VALUE, 2));
				pl.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE, 5));
				pl.sendMessage("Vous êtes désormais invincible!");
				pl.sendMessage("Vous pouvez arrêter l'effet en tapant: /ungodmode");
			} else {
				pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			System.out.println("Cette commande n'est pas éxécutable depuis la Live Console.");
		}
		
		return false;
	}

}
