package io.github.Pixelyyy.goldenplugin.commands;

import io.github.Pixelyyy.goldenplugin.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Prefix implements CommandExecutor {

    private Main main;

    public Prefix(Main main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            Player pl = (Player)commandSender;

            if (GroupManager.isInGroup(pl, "vip")) {
                if(strings[0].equals("set")) {
                    pl.setPlayerListName(main.getConfig().getString(strings[1]).replaceAll("&", "§") +" "+ pl.getName());
                    pl.setCustomName(main.getConfig().getString(strings[1]).replaceAll("&", "§") +" "+ pl.getName());
                    pl.setCustomNameVisible(true);
                }
            } else {
                pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
            }
        } else {
            System.out.println("Cette commande n'est pas éxécutable depuis la Live Console.");
        }
        return false;
    }
}
