package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ResponseCheck implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if (!(arg0 instanceof Player)) {
			for(int i = 0; i < 6; i++) {
				arg0.sendMessage("Si tu as reçu ce message super vite, 5 fois, c'est que le serveur va assez vite.");
			}
		} else {
			arg0.sendMessage("Cette commande ne peut être éxécutée depuis la Console.");
		}

		return false;
	}

}
