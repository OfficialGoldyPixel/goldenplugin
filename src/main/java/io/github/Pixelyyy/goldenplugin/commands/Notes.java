package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Notes implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if (arg0 instanceof Player) {
			Player pl = (Player)arg0;
			if (GroupManager.isInGroup(pl, "vip")) {
				arg0.sendMessage("Il y a " + Note.noteTable.size() + " (en partant de 0) notes :");

				arg0.sendMessage(Note.noteTable.toString());
			} else {
				pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			arg0.sendMessage("Il y a " + Note.noteTable.size() + " (en partant de 0) notes :");

			arg0.sendMessage(Note.noteTable.toString());
		}

		return false;
	}

}
