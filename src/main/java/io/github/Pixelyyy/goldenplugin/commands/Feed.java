package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Feed implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player) {
            try {
                Player pl = (Player)commandSender;

                if (GroupManager.isInGroup(pl, "admin")) {
                    Bukkit.getPlayer(strings[0]).setFoodLevel(20);
                    commandSender.sendMessage(ChatColor.GREEN + "Vous avez nourri " + strings[0] + ChatColor.RESET);
                } else {
                    pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
                }
            } catch(NullPointerException e) {
                commandSender.sendMessage("Ce joueur n'est pas en ligne ou n'existe pas!");
            } catch(ArrayIndexOutOfBoundsException e) {
                Player pl = (Player)commandSender;
                if (GroupManager.isInGroup(pl, "vip")) {
                    pl.setFoodLevel(20);
                    pl.sendMessage(ChatColor.GREEN + "Vous avez été nourri!" + ChatColor.RESET);
                } else {
                    pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
                }
            }


        } else {
            System.out.println("Cette commande n'est pas éxécutable depuis la Live Console.");
        }
        return false;
    }
}
