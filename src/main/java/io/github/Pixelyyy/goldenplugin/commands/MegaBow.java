package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class MegaBow implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player) {
            Player pl = (Player)commandSender;
            if (GroupManager.isInGroup(pl, "admin")) {
                Inventory plInv = pl.getInventory();

                ItemStack mb = new ItemStack(Material.BOW);
                ItemMeta mbm = mb.getItemMeta();

                mbm.setDisplayName("MegaBow");
                mbm.addEnchant(Enchantment.ARROW_DAMAGE, 32000, true);
                mbm.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
                mbm.addEnchant(Enchantment.ARROW_FIRE, 1, true);
                mbm.setUnbreakable(true);

                mbm.setLore(Arrays.asList("L'arc revisité pour les admins!"));

                mb.setItemMeta(mbm);

                ItemStack arrow = new ItemStack(Material.ARROW, 1);

                plInv.addItem(mb);
                plInv.addItem(arrow);
                pl.updateInventory();
            } else {
                pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
            }
        } else {
            System.out.println("Cette commande n'est pas éxécutable depuis la Live Console.");
        }
        return false;
    }
}
