package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EnderChestSee implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if(arg0 instanceof Player) {
			Player pl = (Player)arg0;
			pl.openInventory(pl.getEnderChest());
		} else {
			System.out.println("Cette commande n'est pas éxécutable depuis la Live Console.");
		}
		return false;
	}

}
