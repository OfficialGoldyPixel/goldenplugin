package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Up implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		
		if(arg0 instanceof Player) {
			Player player = (Player)arg0;
			if (GroupManager.isInGroup(player, "admin")) {
				Location playerLocation = player.getLocation();
				Location teleportation = new Location(player.getWorld(), playerLocation.getX(), playerLocation.getY() + 30, playerLocation.getZ());
				player.sendMessage("Tu as été téléporté 30 blocks au-dessus de toi!");
				player.teleport(teleportation);
			} else {
				player.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			System.out.println("Cette commande n'est pas éxécutable dans la Live Console.");
		}
		
		return false;
	}

}
