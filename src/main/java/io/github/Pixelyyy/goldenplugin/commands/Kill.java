package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Kill implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            Player pl = (Player)commandSender;
            if (GroupManager.isInGroup(pl, "admin")) {
                try {
                    String askedPlName = strings[0];
                    Bukkit.getPlayer(askedPlName).setHealth(0);
                } catch(NullPointerException e) {
                    commandSender.sendMessage("Ce joueur n'existe pas ou n'est pas en ligne!");
                } catch(ArrayIndexOutOfBoundsException e) {
                    commandSender.sendMessage("Pas assez ou trop d'arguments! Utilisation : /silentkill <joueur>");
                }
            } else {
                pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
            }
        } else {
            try {
                String askedPlName = strings[0];
                Bukkit.getPlayer(askedPlName).setHealth(0);
            } catch(NullPointerException e) {
                commandSender.sendMessage("Ce joueur n'existe pas ou n'est pas en ligne!");
            } catch(ArrayIndexOutOfBoundsException e) {
                commandSender.sendMessage("Pas assez ou trop d'arguments! Utilisation : /silentkill <joueur>");
            }
        }

        return false;
    }
}
