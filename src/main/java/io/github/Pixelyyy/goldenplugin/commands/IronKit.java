package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class IronKit implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		
		if(arg0 instanceof Player) {
			Player pl = (Player)arg0;
			
			ItemStack sword = new ItemStack(Material.IRON_SWORD);
			ItemMeta swordM = sword.getItemMeta();
			
			swordM.setUnbreakable(true);
			sword.setItemMeta(swordM);
			
			ItemStack pickaxe = new ItemStack(Material.IRON_PICKAXE);
			ItemMeta pickaxeM = sword.getItemMeta();
			
			pickaxeM.setUnbreakable(true);
			pickaxe.setItemMeta(pickaxeM);
			
			ItemStack axe = new ItemStack(Material.IRON_AXE);
			ItemMeta axeM = sword.getItemMeta();
			
			axeM.setUnbreakable(true);
			axe.setItemMeta(axeM);
			
			ItemStack shovel = new ItemStack(Material.IRON_SHOVEL);
			ItemMeta shovelM = sword.getItemMeta();

			shovelM.setUnbreakable(true);
			shovel.setItemMeta(shovelM);
			
			ItemStack helmet = new ItemStack(Material.IRON_HELMET);
			ItemMeta helmetM = helmet.getItemMeta();
			
			helmetM.setUnbreakable(true);
			helmet.setItemMeta(helmetM);
			
			ItemStack tunic = new ItemStack(Material.IRON_CHESTPLATE);
			ItemMeta tunicM = tunic.getItemMeta();
			
			tunicM.setUnbreakable(true);
			tunic.setItemMeta(tunicM);
			
			ItemStack pants = new ItemStack(Material.IRON_LEGGINGS);
			ItemMeta pantsM = pants.getItemMeta();
			
			pantsM.setUnbreakable(true);
			pants.setItemMeta(pantsM);
			
			ItemStack boots = new ItemStack(Material.IRON_BOOTS);
			ItemMeta bootsM = pants.getItemMeta();
			
			bootsM.setUnbreakable(true);
			boots.setItemMeta(bootsM);
			
			ItemStack shield = new ItemStack(Material.SHIELD);
			ItemMeta shieldM = shield.getItemMeta();
			
			shieldM.setDisplayName("Le bouclier de la mort");
			shieldM.setUnbreakable(true);
			shield.setItemMeta(shieldM);
			
			ItemStack potato = new ItemStack(Material.BAKED_POTATO, 5);
			
			pl.getInventory().addItem(sword);
			pl.getInventory().addItem(pickaxe);
			pl.getInventory().addItem(axe);
			pl.getInventory().addItem(shovel);
			pl.getInventory().addItem(potato);
			
			pl.getInventory().addItem(helmet);
			pl.getInventory().addItem(tunic);
			pl.getInventory().addItem(pants);
			pl.getInventory().addItem(boots);
			
			pl.getInventory().addItem(shield);
			
			pl.sendMessage("§eWhoosh!");
		} else {
			System.out.println("Cette commande n'est pas éxécutable depuis la Live Console");
		}
		
		
		return false;
	}

}
