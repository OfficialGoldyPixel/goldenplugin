package io.github.Pixelyyy.goldenplugin.commands;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SuperSword implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		
		if(arg0 instanceof Player) {
			Player player = (Player)arg0;

			if (GroupManager.isInGroup(player, "admin")) {
				ItemStack supersword = new ItemStack(Material.DIAMOND_SWORD, 1);
				ItemMeta superswordmeta = supersword.getItemMeta();

				superswordmeta.setUnbreakable(true);
				superswordmeta.setDisplayName("§cSuperSword");
				superswordmeta.setLore(Arrays.asList("L'épée revisitée pour les admins"));

				superswordmeta.addEnchant(Enchantment.DAMAGE_ALL, 32000, true);
				superswordmeta.addEnchant(Enchantment.FIRE_ASPECT, 5, true);
				superswordmeta.addEnchant(Enchantment.KNOCKBACK, 100, true);
				superswordmeta.addEnchant(Enchantment.LOOT_BONUS_MOBS, 5, true);

				supersword.setItemMeta(superswordmeta);

				player.getInventory().addItem(supersword);
				player.updateInventory();
			} else {
			}
		} else {
			arg0.sendMessage("Cette commande n'est pas éxécutable depuis la Live Console.");
		}
		
		return false;
	}

}
