package io.github.Pixelyyy.goldenplugin.commands;

import io.github.Pixelyyy.goldenplugin.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Maintenance implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player) {
            Player pl = (Player)commandSender;
            if (GroupManager.isInGroup(pl, "superadmin")) {
                if(Main.isOnMaintenance()) {
                    Main.maintenance(false);
                    commandSender.sendMessage("Le serveur " + ChatColor.GREEN + "n'est plus" + ChatColor.RESET + " verouillé.");
                } else {
                    Main.maintenance(true);
                    commandSender.sendMessage("Le serveur " + ChatColor.RED + "est" + ChatColor.RESET + " désormais verouillé");
                }
            } else {
                pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
            }
        } else {
            if(Main.isOnMaintenance()) {
                Main.maintenance(false);
                commandSender.sendMessage("Le serveur " + ChatColor.GREEN + "n'est plus" + ChatColor.RESET + " verouillé.");
            } else {
                Main.maintenance(true);
                commandSender.sendMessage("Le serveur " + ChatColor.RED + "est" + ChatColor.RESET + " désormais verouillé");
            }
        }

        return false;
    }
}
