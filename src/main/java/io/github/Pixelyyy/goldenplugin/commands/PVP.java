package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PVP implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player) {
            Player pl = (Player)commandSender;
            if (GroupManager.isInGroup(pl, "superadmin")) {
                if(pl.getWorld().getPVP()) {
                    pl.getWorld().setPVP(false);
                    pl.sendMessage("Le PVP " + ChatColor.GREEN + "est désactivé" + ChatColor.RESET + ".");
                } else {
                    pl.getWorld().setPVP(true);
                    pl.sendMessage("Le PVP " + ChatColor.RED + "est activé" + ChatColor.RESET + ".");
                }
            } else {
                pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
            }
        } else {
            System.out.println("Ctte commande n'est pas éxécutable depuis la Live Console.");
        }
        return false;
    }
}
