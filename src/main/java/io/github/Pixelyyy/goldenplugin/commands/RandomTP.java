package io.github.Pixelyyy.goldenplugin.commands;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RandomTP implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		
		if(arg0 instanceof Player) {
			Player player = (Player)arg0;

			if (GroupManager.isInGroup(player, "vip")) {
				Random rand1 = new Random();
				Random rand2 = new Random();

				Location plloc = player.getLocation();
				Location teleportation = new Location(player.getWorld(), plloc.getX() + rand1.nextInt(100), plloc.getY(), plloc.getZ() + rand2.nextInt(100));

				player.teleport(teleportation);
			} else {
				player.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			System.out.println("Cette commande n'est pas éxécutable dans la Live Console.");
		}
		
		return false;
	}

}
