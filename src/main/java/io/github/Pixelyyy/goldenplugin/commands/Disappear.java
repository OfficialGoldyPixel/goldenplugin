package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Disappear implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player) {
            Player pl = (Player)commandSender;

            if (GroupManager.isInGroup(pl, "admin")) {
                pl.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 2));
                pl.sendMessage(ChatColor.YELLOW + "Whoosh!" + ChatColor.RESET);
            } else {
                pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
            }
        } else {
            System.out.println("Cette commande n'est pas éxécutable depuis la Live Console");
        }
        return false;
    }
}
