package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InventorySee implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if(arg0 instanceof Player) {
			if(arg3.length == 1) {
				Player pl = (Player)arg0;

				if (GroupManager.isInGroup(pl, "admin")) {
					String askedPlayer = arg3[0];
					System.out.println("Asked command from invsee is " + askedPlayer);
					Player askedPl = Bukkit.getPlayer(askedPlayer);

					pl.openInventory(askedPl.getInventory());
					askedPl.updateInventory();
				} else {
					pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
				}
			} else {
				arg0.sendMessage("Pas assez ou trop d'arguments! Utilisation : /invsee <joueur>");
			}
		} else {
			System.out.println("Cette commande n'est pas éxécutable depuis la Live Console.");
		}
		return false;
	}

}
