package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

public class DisInvincible implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if(arg0 instanceof Player) {
			Player pl = (Player)arg0;
			if (GroupManager.isInGroup(pl, "admin")) {
				pl.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
				pl.removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
				pl.removePotionEffect(PotionEffectType.WATER_BREATHING);
				pl.removePotionEffect(PotionEffectType.FAST_DIGGING);
				pl.removePotionEffect(PotionEffectType.REGENERATION);
				pl.removePotionEffect(PotionEffectType.DOLPHINS_GRACE);
				pl.removePotionEffect(PotionEffectType.HEALTH_BOOST);
			} else {
				pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			System.out.println("Cette commande n'est pas éxécutable depuis la Live Console.");
		}
		return false;
	}

}
