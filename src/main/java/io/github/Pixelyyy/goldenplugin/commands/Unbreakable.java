package io.github.Pixelyyy.goldenplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Unbreakable implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if(arg0 instanceof Player) {
			Player pl = (Player)arg0;

			if (GroupManager.isInGroup(pl, "vip")) {
				ItemStack target = pl.getInventory().getItemInMainHand();
				ItemMeta targetM = target.getItemMeta();

				targetM.setUnbreakable(true);
				target.setItemMeta(targetM);

				pl.sendMessage("L'item que tu as dans la main est désormais incassable!");
			} else {
				pl.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ça!" + ChatColor.RESET);
			}
		} else {
			System.out.println("Cette commande n'est pas éxécutable dans la Live Console.");
		}
		return false;
	}

}
