package io.github.Pixelyyy.goldenplugin.dependencies

import org.bukkit.Material
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta

class Item(_material: Material, _amount: Int) {
    private var material: Material = _material
    private var amount: Int = _amount

    private var item: ItemStack
    private var itemmeta: ItemMeta?

    init {
        item = ItemStack(material, amount)
        itemmeta = item.itemMeta
    }

    fun get(): ItemStack {
        return item
    }

    fun enchant(ench:Enchantment, level:Int, active:Boolean) {
        itemmeta?.addEnchant(ench, level, active)
    }

    fun setDisplayName(str: String) {
        itemmeta?.setDisplayName(str)
    }

    /**
     * This method can only be run one time per Object.
     */
    fun applyChanges() {
        item.itemMeta = itemmeta
    }

    fun isUnbreakable(b: Boolean) {
        itemmeta?.isUnbreakable = b
    }

    fun give(inv: Inventory) {
        inv.addItem(item)
    }

    fun set(inv: Inventory, slot: Int) {
        inv.setItem(slot, item)
    }
}