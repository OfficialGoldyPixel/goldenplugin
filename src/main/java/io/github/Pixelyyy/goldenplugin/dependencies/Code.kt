package io.github.Pixelyyy.goldenplugin.dependencies

fun repeat(amount: Int, loopMethod: ILoopMethod, args: Array<Any>) {
    (0..amount).forEach() { i ->
        loopMethod.method(args, amount)
    }
}