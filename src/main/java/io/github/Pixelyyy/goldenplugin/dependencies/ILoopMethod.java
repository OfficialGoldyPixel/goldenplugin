package io.github.Pixelyyy.goldenplugin.dependencies;

public interface ILoopMethod {
    void method(Object[] args, int amount);
}
