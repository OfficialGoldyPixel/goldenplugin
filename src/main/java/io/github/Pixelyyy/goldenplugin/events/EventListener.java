package io.github.Pixelyyy.goldenplugin.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.github.Pixelyyy.goldenplugin.Main;
import io.github.Pixelyyy.goldenplugin.commands.GroupManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class EventListener implements org.bukkit.event.Listener {
	
	public Inventory inv1 = Bukkit.createInventory(null, 27, "§8Menu Admin (1/2)");
	public Inventory inv2 = Bukkit.createInventory(null, 27, "§8Menu Admin (2/2)");
	public static Inventory trash = Bukkit.createInventory(null, 54, "Poubelle");
	public static Inventory cs = Bukkit.createInventory(null, 54, "Stockage de communauté");

	private Main main;

	/**
	 * Liste des joueurs en java.lang.String
	 */
	public static ArrayList<String> onlinePl = new ArrayList<String>();

	/**
	 * Liste des joueurs en org.bukkit.entity.Player
	 */
	public static Map<String, Player> plMap = new HashMap<>();

	/**
	 * Groupes & noeuds de permissions
	 */
	public static Map<Player, Boolean> superAdminGroup = new HashMap<>();
	public static Map<Player, Boolean> devGroup = new HashMap<>();
	public static Map<Player, Boolean> adminGroup = new HashMap<>();
	public static Map<Player, Boolean> vipGroup = new HashMap<>();
	public static Map<Player, Boolean> memberGroup = new HashMap<>();

	public EventListener(Main main) {
		this.main = main;
	}

	@EventHandler
	public void onEntityJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		if(!Main.isOnMaintenance()) {
			e.setJoinMessage(ChatColor.YELLOW + player.getName() + " à rejoint le serveur!" + ChatColor.RESET);

			onlinePl.add(player.getName());
			plMap.put(player.getName(), player);

			player.setPlayerListName("DonutLand");

			try {
				System.out.println("Config detected for player " + player.getName() + ".");
				GroupManager.addToGroups(player, main.getConfig().getString("players." + player.getName() + ".groups").split(","));
			} catch (NullPointerException ex) {
				System.out.println("No config detected for player" + player.getName() + ".");
			}

			if(GroupManager.isInGroup(player, "vip")) {
				player.setPlayerListName(main.getConfig().getString("prefix.vip").replaceAll("&", "§") +" "+ player.getName());
				player.setCustomName(main.getConfig().getString("prefix.vip") +" "+ player.getName());
				player.setCustomNameVisible(true);
			}

			if(GroupManager.isInGroup(player, "admin")) {
				player.setPlayerListName(main.getConfig().getString("prefix.admin").replaceAll("&", "§") +" "+ player.getName());
				player.setCustomName(main.getConfig().getString("prefix.admin") +" "+ player.getName());
				player.setCustomNameVisible(true);
			}

			if(GroupManager.isInGroup(player, "superadmin")) {
				player.setPlayerListName(main.getConfig().getString("prefix.superadmin").replaceAll("&", "§") +" "+ player.getName());
				player.setCustomName(main.getConfig().getString("prefix.superadmin") +" "+ player.getName());
				player.setCustomNameVisible(true);
			}

			if(GroupManager.isInGroup(player, "dev")) {
				player.setPlayerListName(main.getConfig().getString("prefix.dev").replaceAll("&", "§") +" "+ player.getName());
				player.setCustomName(main.getConfig().getString("prefix.dev") +" "+ player.getName());
				player.setCustomNameVisible(true);
			}

			if(onlinePl.size() == 1) {
				player.sendMessage(ChatColor.YELLOW + "Bienvenue sur le serveur! Il y a " + onlinePl.size() + " joueur en ligne. Fais /onlinepl pour savoir qui est en ligne!" + ChatColor.RESET);
			} else {
				player.sendMessage(ChatColor.YELLOW + "Bienvenue sur le serveur! Il y a " + onlinePl.size() + " joueurs en ligne. Fais /onlinepl pour savoir qui est en ligne!" + ChatColor.RESET);
			}
		} else {
			Bukkit.broadcastMessage(ChatColor.RED + player.getName() + " à essayé de rejoindre, mais le serveur est verouillé." + ChatColor.RESET);
			player.kickPlayer("Le serveur est actuellement verouillé! Veuillez revenir plus tard.");
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		Action action = e.getAction();
		ItemStack item = e.getItem();
		
		if(item == null) return;
		
		if(action == Action.RIGHT_CLICK_AIR && item.getType() == Material.COMPASS) {
			Location locateplayer = player.getLocation();
			Location spawn = player.getWorld().getSpawnLocation();
			
			player.sendMessage("La boussole dit 'Le spawn est à " + spawn.getX() + ", " + spawn.getY() + ", " + spawn.getZ() + "!'");
		}
		
		if(action == Action.RIGHT_CLICK_AIR && item.getType() == Material.DIAMOND_AXE) {
			if(item.getItemMeta().hasEnchant(Enchantment.DAMAGE_ALL)) {
				ItemStack gm1 = new ItemStack(Material.DIAMOND);
				ItemMeta gm1meta = gm1.getItemMeta();
				
				gm1meta.setDisplayName("Passage en mode créatif");
				gm1meta.addEnchant(Enchantment.DAMAGE_ALL, 5, true);
				gm1.setItemMeta(gm1meta);
				
				ItemStack day = new ItemStack(Material.CLOCK);
				ItemMeta daymeta = day.getItemMeta();
				
				daymeta.setDisplayName("Mettre le jour");
				day.setItemMeta(daymeta);
				
				ItemStack gm0 = new ItemStack(Material.IRON_SWORD);
				ItemMeta gm0meta = gm0.getItemMeta();
				
				gm0meta.setDisplayName("Passer en mode survie");
				gm0.setItemMeta(gm0meta);
				
				ItemStack godm = new ItemStack(Material.NETHER_STAR);
				ItemMeta godmmeta = godm.getItemMeta();
				
				godmmeta.setDisplayName("Passer en godmode");
				godm.setItemMeta(godmmeta);
				
				ItemStack spawn = new ItemStack(Material.COMPASS);
				ItemMeta spawnmeta = spawn.getItemMeta();
				
				spawnmeta.setDisplayName("Retourner au spawn du serveur");
				spawn.setItemMeta(spawnmeta);
				
				ItemStack stoptherain = new ItemStack(Material.SNOW_BLOCK);
				ItemMeta stoptherainM = stoptherain.getItemMeta();
				
				stoptherainM.setDisplayName("Arrêter la pluie");
				stoptherain.setItemMeta(stoptherainM);
				
				ItemStack ss = new ItemStack(Material.DIAMOND_SWORD);
				ItemMeta ssmeta = ss.getItemMeta();
				
				ssmeta.setDisplayName("Obtenir la super sword");
				ss.setItemMeta(ssmeta);
				
				ItemStack nextpage1 = new ItemStack(Material.ARROW);
				ItemMeta nextpagemeta = nextpage1.getItemMeta();
				
				nextpagemeta.setDisplayName("Page suivante");
				nextpage1.setItemMeta(nextpagemeta);
				
				
				inv1.setItem(16, ss);
				inv1.setItem(15, stoptherain);
				inv1.setItem(14, spawn);
				inv1.setItem(13, godm);
				inv1.setItem(12, day);
				inv1.setItem(11, gm0);
				inv1.setItem(10, gm1);
				inv1.setItem(26, nextpage1);
				
				
				player.openInventory(inv1);
				
				ItemStack previouspage1 = new ItemStack(Material.STONE_SWORD);
				ItemMeta previouspage1M = previouspage1.getItemMeta();
				
				previouspage1M.setDisplayName("Page précédente");
				previouspage1.setItemMeta(previouspage1M);
				
				ItemStack trash = new ItemStack(Material.LAVA_BUCKET);
				ItemMeta trashM = trash.getItemMeta();
				
				trashM.setDisplayName("Ouvrir la poubelle");
				trash.setItemMeta(trashM);
				
				ItemStack communitystorage = new ItemStack(Material.BLACK_SHULKER_BOX);
				ItemMeta csM = communitystorage.getItemMeta();
				
				csM.setDisplayName("Ouvrir le stockage de communauté");
				communitystorage.setItemMeta(csM);
				
				ItemStack enderchest = new ItemStack(Material.ENDER_CHEST);
				ItemMeta enderchestM = enderchest.getItemMeta();
				
				enderchestM.setDisplayName("Ouvrir l'Ender Chest");
				enderchest.setItemMeta(enderchestM);

				ItemStack mb = new ItemStack(Material.BOW);
				ItemMeta mbm = mb.getItemMeta();

				mbm.setDisplayName("Obtenir le MegaBow");
				mbm.addEnchant(Enchantment.KNOCKBACK, 10, true);
				mbm.addEnchant(Enchantment.ARROW_FIRE, 1, true);

				mb.setItemMeta(mbm);

				inv2.setItem(13, mb);
				inv2.setItem(12, enderchest);
				inv2.setItem(11, communitystorage);
				inv2.setItem(10, trash);
				inv2.setItem(18, previouspage1);
			}
		}
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Inventory inv = e.getInventory();
		Player pl = (Player)e.getWhoClicked();
		ItemStack current = e.getCurrentItem();
		InventoryView inve = e.getView();
		
		if(current == null) return;
		
		if(e.getView().getTitle().equals("§8Menu Admin (1/2)")) {
			e.setCancelled(true);
			pl.closeInventory();
			System.out.println(pl.getName() + " clicked somewhere in the Admin Menu inventory");
			
			switch(current.getType()) {
			case DIAMOND:
				pl.chat("/minecraft:gamemode creative");
				break;
			case CLOCK:
				pl.chat("/minecraft:time set day");
				break;
			case IRON_SWORD:
				pl.chat("/minecraft:gamemode survival");
				break;
			case NETHER_STAR:
				pl.chat("/goldenplugin:godmode");
				break;
			case COMPASS:
				pl.chat("/goldenplugin:spawn");
				break;
			case DIAMOND_SWORD:
				pl.chat("/goldenplugin:supersword");
				break;
			case ARROW:
				pl.openInventory(inv2);
				break;
			}
		}
		
		if(e.getView().getTitle().equals("§8Menu Admin (2/2)")) {
			e.setCancelled(true);
			pl.closeInventory();
			System.out.println(pl.getName() + " clicked somewhere in the Admin Menu inventory");
			
			switch(current.getType()) {
			case STONE_SWORD:
				pl.openInventory(inv1);
				break;
			case LAVA_BUCKET:
				pl.openInventory(trash);
				trash.clear();
				break;
			case BLACK_SHULKER_BOX:
				pl.openInventory(cs);
				break;
			case ENDER_CHEST:
				pl.openInventory(pl.getEnderChest());
				break;
			case BOW:
				pl.chat("/goldenplugin:megabow");
				break;
			}
		}
	}
	
	@EventHandler
	public void onEntityLeft(PlayerQuitEvent e) {
		Player pl = e.getPlayer();
		e.setQuitMessage(ChatColor.YELLOW + pl.getName() + " à quitté." + ChatColor.RESET);
		onlinePl.remove(pl.getName());
		plMap.remove(pl.getName());
	}
	
	@EventHandler
	public void onEntityGotOnBed(PlayerBedEnterEvent e) {
		Player pl = e.getPlayer();
		Bukkit.broadcastMessage(ChatColor.BLUE + pl.getName() + " est parti dormir." + ChatColor.RESET);
	}
}
